package stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import seleniumPages.Page_GoogleHomePage;



    public class StepDefs_GoogleHomePage {
	Page_GoogleHomePage HomePage = new Page_GoogleHomePage();
	
	@Given("^I launch Chrome browser$")
	public void i_launch_Chrome_browser() {
	   System.out.println("launch google Chrome");
	   HomePage.launchBrowser();
	}

	@When("^I open Google Homepage$")
	public void i_open_Google_Homepage() {
		 System.out.println("launch google home page");
	     HomePage.openGoogleURL();
	}

	@Then("^I verify that the page displays search text box$")
	public void i_verify_that_the_page_displays_search_text_box() {
	    System.out.println("display search box");
        HomePage.checkSearchBoxIsDisplayed();
	}

	@Then("^the page displays Google Search button$")
	public void the_page_displays_Google_Search_button() {
	    System.out.println("google search button is displayed in the page");
		HomePage.checkGoogleSearchButtonIsDisplayed();
	}

	@Then("^the page displays Im Feeling Lucky button$")
	public void the_page_displays_Im_Feeling_Lucky_button() {
	    System.out.println("the page displays I am Feeling Lucky button");
	    HomePage.checkImFeelingLuckyButtonIsDisplayed();
	}

}
