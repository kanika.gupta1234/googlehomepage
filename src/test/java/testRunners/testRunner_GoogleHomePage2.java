

package testRunners;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
  plugin = {"pretty"}, 
  features = {"classpath:features/"},
  glue = {"stepDefinitions"}
)
public class testRunner_GoogleHomePage2 {
 
}

